console.log("Hello Wolrd!");

// [SECTION] Syntax, Statements, and Comments

// To add comment, we are using "//" or CTRL + / -> single line comment

/*

This
is
a
multi
line
comment

CTRL + SHIFT + /

*/

// SYNTAX AND STATEMENTS
// JS Statements usually ends with a semi-colon (;)
// Statements -> in programming are instruction that we tell our computer to perform.
// Semi-colons are not required in JS, but we will us it to help us train to locate where a statement ends.

// [SECTION] Variables
// It is used to contain data.
// Any information that is used by an application is stored in what we call a "memory"
// When we create variables, certain portions of a device's memory is given a "name" that we call "variables"
// This makes it easier for us associate information stored in our devices to actual "names" about information

// Variable Declaration
// Declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it with the value of "undefined", meaning the variable's value was "not defined".

// Syntax -> var/let/const variableName;

// Varialble names should be unique and cannot be declared multiple times.

let myVariable;

console.log(myVariable);

// Variable should be declared first before they are used.
// Using variable before they were declared will return an error.
let hello;
console.log(hello);

// CAMEL CASING -> thisIsCamelCasing
// SNAKE CASING -> this_is_snake_casing
// KEBAB CASING -> this-is-kebab-casing

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
*/

// Declaring and Initializing Variable
// let/const variableName = value;

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain applications, some variables/information are constant and should not be changed
// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended

const interest = 3.539;
const hoursInADay = 24;

// Re-assign a value
// Changing it's initial or previous value into another value
// Syntax -> variableName = newValue;

productName = "Laptop";
console.log(productName);

/*interest = 5.2;
console.log(interest);*/

// let vs conts
// let -> variable values can be changed or updated
// const -> variable values cannot be updated

// Re-assigning vs Initializing Variable
// Declare variable first
let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

/*
Will cause error because the const is missing initialization

const pi;
pi = 3.1416;
console.log(pi);
*/

// HOISTING -> default behaviour of moving declaration on the top.

a = 5;
console.log(a);
var a;

// Multiple Variable Declarations
// Usually declared in one line
// Usually quicker

/*
Individual Declaration

let productCode = "DC017";
let productBrand = "Dell";*/

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);

// [SECTION] Data Types
// Strings
// Strings are series of characters that creats a word, a phrase, a sentence or anything related to creating text.
// Strings in JS can be written using sing ('') or double ("") quote
// In other programming languages, only the double quotes used for creating strings.

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating Strings
// Multiple Strings can be combined to create a single string using the plus "+" symbol.

// Sample output -> Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

// Escape Characters (\)
// "\n" refers to creating a new line

let mailAddress = "Metro Manila \n\n Philippines"
console.log(mailAddress);

// Using double quotes along with single quotes
let message = "John's employees went home early";
console.log(message);

message = 'John\'s employees went home';
console.log(message);

// Numbers
// Intergers/Whole Numbers

let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation
let planetDistance = 2e20;
console.log(planetDistance);

// Combining of text and int
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to store values relating to the state of certain thing.
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried? " + isMarried);
console.log("inGoodConduct? " + inGoodConduct);

// Arrays
// Arrays are a special kind of data type that's used to store multiple values.
// Arrays can store different data types but is normally used to store similar data types.
// Syntax let/const arrayName = [valueA, valueB, valueC...];

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

// different data types
let details = ["John", "Smith", 32, true];
console.log(details);

// Objects
// Objects are another special kinds of data type
// It has properties and values -> key pairs
/*

let/const objectName = {
	propertyA: value,
	propertyB: value
}

*/

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+6312345", "+6398765"],
	address: {
		houseNumber: "345",
		city: "Manila"
	}
}

console.log(person);

// They're also useful for creating abstract objects
let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 98.2,
	fourthGrading: 94.6
}

console.log(myGrades);

// "typeof" is used to determine the type of data of a variable.
console.log(typeof isMarried);
console.log(typeof person);
console.log(typeof grades);

// Constant Object and Arrays

const anime = ["one piece", "once punch man", "attack on titan"];
anime[0] = "Kimetsu No Yaiba";
console.log(anime);